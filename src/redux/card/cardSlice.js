import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  card: [],
  sum: 0,
  difference: 0,
};

const cardSlice = createSlice({
  name: "card",
  initialState,
  reducers: {
    // добавление товара в корзину
    cardAdded: {
      reducer(state, action) {
        state.card.push(action.payload);
        state.sum += Number(action.payload.price);
      },
      prepare(nanoid, title, img, price) {
        return {
          payload: {
            id: nanoid,
            title,
            img,
            price,
            count: 1,
          },
        };
      },
    },
    // удаление товара из корзины
    cardDeleted: {
      reducer(state, action) {
        let arr = state.card.filter((item) => item.id !== action.payload.id);
        state.card = arr;

        state.card.map((el) => {
          if (action.payload.id) {
            if (state.card.length === 0) {
              state.sum = 0;
            } else if (state.card.length !== 0) {
              state.difference =
                state.sum - Number(action.payload.price) * action.payload.count;
              state.sum = state.difference;
            } else if (state.card.length !== 0 && action.payload.count > 1) {
              state.difference =
                state.sum - Number(action.payload.price) * action.payload.count;
              state.sum -= state.difference;
            } else {
              state.sum = state.sum - Number(action.payload.price);
            }
          }
          return state;
        });
      },
      prepare(id, title, img, price, countState) {
        return {
          payload: {
            id,
            title,
            img,
            price,
            count: countState,
          },
        };
      },
    },
    // счетчик уменьшения количества товара
    countItemMin: {
      reducer(state, action) {
        state.card.map((el) => {
          if (el.id === action.payload.id) {
            state.sum = state.sum - Number(action.payload.price);
            el.count -= action.payload.count;
          }
          return state;
        });
      },
      prepare(id, price, countState) {
        return {
          payload: {
            id,
            price,
            count: countState,
          },
        };
      },
    },
    // счетчик увеличения количества товара
    countItemAdd: {
      reducer(state, action) {
        state.card.map((el) => {
          if (el.id === action.payload.id) {
            state.sum = state.sum + Number(action.payload.price);
            el.count += action.payload.count;
          }
          return state;
        });
      },
      prepare(id, price, countState) {
        return {
          payload: {
            id,
            price,
            count: countState,
          },
        };
      },
    },
  },
});

export const { cardAdded, cardDeleted, countItemMin, countItemAdd } =
  cardSlice.actions;
export const stateAllCard = (state) => state.card.card;
export const stateAllSum = (state) => state.card.sum;
//export const stateAllCount = state => state.card.count

export const cardReducer = cardSlice.reducer;
