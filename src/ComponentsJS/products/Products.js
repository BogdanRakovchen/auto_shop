//--------------react-------------------
import React, { useEffect, useState } from "react";
//-------------material-ui--------------
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import TextField from "@mui/material/TextField";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
//------------Component-----------------
import Cards from "../order/Cards";
//------------redux---------------------
import { useSelector, useDispatch } from "react-redux";
import {
  fetchPosts,
  firstFourProducts,
  allProducts,
  secondFourProducts,
  threeFourProducts,
} from "../../redux/product/productsSlice";

const Products = () => {
  const dispatch = useDispatch();

  const productsStatus = useSelector((state) => state.products.status);
  const products = useSelector(allProducts);
  let productsFirst = useSelector(firstFourProducts);
  let productsSecond = useSelector(secondFourProducts);
  let productsThird = useSelector(threeFourProducts);

  useEffect(() => {
    if (productsStatus === "idle") {
      dispatch(fetchPosts());
    }
  }, [productsStatus, dispatch]);

  const [page, setPage] = useState(1);

  const [value, setValue] = useState("");
  const [checkAudi, setCheckAudi] = useState(false);
  const [checkBmw, setCheckBmw] = useState(false);
  const [checkNissan, setCheckNissan] = useState(false);

  let [fourPaginationPage, setFourPaginationPage] = useState(false);

  const changePaginationPage = (event, value) => {
    setPage(value);
    let scrollElm = document.scrollingElement;
    scrollElm.scrollTop = 0;
  };

  let audi = productsFirst.map((prod) => {
    return <Cards key={prod.id} {...prod} />;
  });

  const handleCheckboxAudi = () => {
    setCheckAudi(!checkAudi);
    if (!checkAudi) {
      setPage(4);
    } else {
      setPage(1);
    }
  };

  let bmw = productsSecond.map((prod) => {
    return <Cards key={prod.id} {...prod} />;
  });

  const handleCheckboxBmw = () => {
    setCheckBmw(!checkBmw);
    if (!checkBmw) {
      setPage(4);
    } else {
      setPage(1);
    }
  };

  let nissan = productsThird.map((prod) => {
    return <Cards key={prod.id} {...prod} />;
  });

  const handleCheckboxNissan = () => {
    setCheckNissan(!checkNissan);
    if (!checkNissan) {
      setPage(4);
    } else {
      setPage(1);
    }
  };

  let pageOne;
  let pageTwo;
  let pageThree;
  let pageFour;

  const handleChangeInput = (event) => {
    setValue(event.target.value);

    setFourPaginationPage(!fourPaginationPage);
  };

  pageFour = products.map((prod) => {
    if (prod.title.toLowerCase().includes(value.toLowerCase())) {
      return <Cards key={prod.id} {...prod} />;
    }
    return true;
  });

  switch (page) {
    case 1:
      pageOne = productsFirst.map((prod) => {
        return <Cards key={prod.id} {...prod} />;
      });
      break;
    case 2:
      pageTwo = productsSecond.map((prod) => {
        return <Cards key={prod.id} {...prod} />;
      });
      break;
    case 3:
      pageThree = productsThird.map((prod) => {
        return <Cards key={prod.id} {...prod} />;
      });
      break;

    default:
      <p>hello world</p>;
      break;
  }

  return (
    <>
      <Box component="section" sx={{ minHeight: "500px" }}>
        <Box component="h2">Наши автомобили</Box>

        <Box component="div" sx={{ display: "flex" }}>
          <Box
            component="aside"
            sx={{
              width: "20%",
              height: "340px",
              border: "1px solid black",
              borderRadius: "5px",
              boxShadow: "3",
              position: "sticky",
              top: "200",
            }}
          >
            <Box
              compnent="h3"
              sx={{
                textAlign: "center",
                paddingTop: "10px",
                fontWeight: "bold",
                color: "blue",
              }}
            >
              Сортировка
            </Box>

            <Box component="div">
              <TextField
                onChange={handleChangeInput}
                value={value}
                sx={{ width: "90%", marginTop: "10px", marginLeft: "5%" }}
                label="Найти автомобиль"
              />
            </Box>

            <Box
              component="div"
              sx={{
                display: "flex",
                flexDirection: "column",
                paddingLeft: "7%",
                paddingTop: "10%",
              }}
            >
              <FormControlLabel
                label="Audi"
                control={<Checkbox />}
                onChange={handleCheckboxAudi}
                checked={checkAudi}
              />
              <FormControlLabel
                label="BMW"
                control={<Checkbox />}
                onChange={handleCheckboxBmw}
                checked={checkBmw}
              />
              <FormControlLabel
                label="Nissan"
                control={<Checkbox />}
                onChange={handleCheckboxNissan}
                checked={checkNissan}
              />
            </Box>
          </Box>

          <Box
            component="div"
            sx={{
              width: "100%",
              minHeight: "450px",
              marginLeft: "5%",
              borderRadius: "10px",
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "space-around",
            }}
          >
            {!fourPaginationPage ? pageOne || pageTwo || pageThree : pageFour}

            {checkAudi ? audi : null}
            {checkBmw ? bmw : null}
            {checkNissan ? nissan : null}
          </Box>
        </Box>
      </Box>

      <Box component="div" sx={{ marginLeft: "52.5%", marginBottom: "32px" }}>
        <Stack spacing={2}>
          <Pagination
            count={3}
            defaultPage={1}
            page={page}
            onChange={changePaginationPage}
            shape="rounded"
          />
        </Stack>
      </Box>
    </>
  );
};

export default Products;
