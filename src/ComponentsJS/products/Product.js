//------------react----------------
import React from "react";
//-----------material-ui-----------
import Box from "@mui/material/Box";
//----------redux-----------------
import { useSelector } from "react-redux";
import { descriptionForProduct } from "../../redux/product/productsSlice";

const Product = () => {
  const allDescriptionForProduct = useSelector(descriptionForProduct);

  const {
    title,
    price,
    engine,
    power,
    transmission,
    driveUnit,
    bodyType,
    color,
    mileage,
    steeringWheel,
    age,
    img,
  } = allDescriptionForProduct;

  return (
    <Box component="section">
      <Box component="h2">{title}</Box>
      <Box
        component="div"
        sx={{ display: "flex", justifyContent: "flex-start" }}
      >
        <Box component="img" width="50%" src={img}></Box>
        <Box component="div" width="100%">
          <Box
            component="span"
            sx={{ fontWeight: "bold", fontSize: "2rem", marginLeft: "16%" }}
          >
            {price} рублей
          </Box>
          <Box component="div">
            <Box
              component="table"
              sx={{ borderSpacing: "30px 15px", marginLeft: "10%" }}
            >
              <Box component="tbody">
                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Двигатель
                  </Box>

                  <Box component="td">{engine}</Box>
                </Box>
                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Мощность
                  </Box>

                  <Box component="td">{power}</Box>
                </Box>
                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Трансмиссия
                  </Box>

                  <Box component="td">{transmission}</Box>
                </Box>
                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Привод
                  </Box>

                  <Box component="td">{driveUnit}</Box>
                </Box>
                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Тип кузова
                  </Box>

                  <Box component="td">{bodyType}</Box>
                </Box>

                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Цвет
                  </Box>

                  <Box component="td">{color}</Box>
                </Box>
                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Пробег, км
                  </Box>

                  <Box component="td">{mileage}</Box>
                </Box>
                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Руль
                  </Box>

                  <Box component="td">{steeringWheel}</Box>
                </Box>
                <Box component="tr">
                  <Box
                    component="td"
                    sx={{ color: "darkgray", fontSize: "19px" }}
                  >
                    Поколение
                  </Box>

                  <Box component="td">{age}</Box>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Product;
