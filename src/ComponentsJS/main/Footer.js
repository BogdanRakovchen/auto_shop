// ----------react---------------
import React from "react";
// ---------material ui-----------
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import Link from "@mui/material/Link";

const Footer = () => {
  return (
    <>
      <CssBaseline />
      <Box
        sx={{
          bgcolor: "blue",
          minHeight: "100px",
          width: "auto",
          display: "flex",
          alignItems: "center",
        }}
      >
        <Container>
          <Stack
            direction="row"
            spacing={{ xs: 5, sm: 15, md: 45, lg: 80, xl: 90 }}
          >
            <Stack
              component="ul"
              direction="row"
              spacing={{ xs: 1, sm: 2, md: 3, lg: 2, xl: 5 }}
            >
              <Typography variant="body2" color="white" align="center">
                {"Copyright © "}
                <Link color="inherit" href="#">
                  Автомагазин
                </Link>{" "}
                {new Date().getFullYear()}
                {"."}
              </Typography>
            </Stack>
          </Stack>
        </Container>
      </Box>
    </>
  );
};

export default Footer;
