// ---------react----------
import React from "react";
// ---------Components----------
import Header from "./main/Header";
import Footer from "./main/Footer";
import Basic from "./main/Basic";
import About from "./main/About";
import Products from "./products/Products";
import Autorization from "./auth/Autorization";
import Registration from "./auth/Registration";
import Product from "./products/Product";
import BuyBasket from "./order/BuyBasket";
import CheckoutOrder from "./order/СheckoutOrder";
// ---------material ui-------------
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Routes, Route } from "react-router-dom";

// -----------localstorage---------------
export let storage = window.localStorage;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      valueStorage: JSON.parse(window.localStorage.getItem("state")),
    };
  }

  render() {
    // функция колбэк(бумеранг) для получения булевского
    //значения в случае авторизации и сохранения в localstorage
    const auth = (valueAuth) => {
      window.localStorage.setItem("state", JSON.stringify(valueAuth));
      this.setState({
        valueStorage: window.localStorage.getItem("state"),
      });
    };

    return (
      <>
        <Header auth={auth} value1={this.state.value1} />
        <Box component="main" minHeight="550px">
          <Container minHeight="600px">
            <Routes>
              <Route path="/" element={<Basic />} />
              <Route path="/about" element={<About />} />
              <Route path="/products" element={<Products />} />
              <Route
                path="/autorization"
                element={<Autorization auth={auth} />}
              />
              <Route path="/registration" element={<Registration />} />
              <Route path="/product" element={<Product />} />
              <Route path="/buybasket" element={<BuyBasket />} />
              <Route path="/checkoutorder" element={<CheckoutOrder />} />
            </Routes>
          </Container>
        </Box>
        <Footer />
      </>
    );
  }
}

export default App;
