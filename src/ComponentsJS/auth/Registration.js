//------------react----------------
import React, { useState } from "react";
//-----------material-ui------------
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
//----------react-router-dom-----------------
import { NavLink, useNavigate } from "react-router-dom";

// для проверки в компоненте Autorization и сохранения состояния сессии
export let myStorage = window.localStorage;

function Copyright(props) {
  return <Typography {...props} />;
}

const theme = createTheme();

const Registarion = () => {
  let navigate = useNavigate();

  let [name, setName] = useState(false);
  let [lastName, setLastName] = useState(false);
  let [email, setEmail] = useState(false);
  let [password, setPassword] = useState(false);

  let regNames = /[A-z]/gi;
  let regLast = /[A-z]/gi;
  let regPassword = /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}/g;
  let regEmail =
    /^((([0-9A-Za-z]{1}[-0-9A-z.]{0,30}[0-9A-Za-z]?)|([0-9А-Яа-я]{1}[-0-9А-я.]{0,30}[0-9А-Яа-я]?))@([-A-Za-z]{1,}\.){1,}[-A-Za-z]{2,})$/;

  // проверка на валидность, когда false инпут подсвечивает красным, если true возвращается к синиму
  const handleChangeFirstName = (e) =>
    regNames.test(e.target.value) ? setName(false) : setName(true);
  const handleChangeLastName = (e) =>
    regLast.test(e.target.value) ? setLastName(false) : setLastName(true);
  const handleChangeEmail = (e) =>
    regEmail.test(e.target.value) ? setEmail(false) : setEmail(true);
  const handleChangePassword = (e) =>
    regPassword.test(e.target.value) ? setPassword(false) : setPassword(true);

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    let firstName = data.get("firstName");
    let lastName = data.get("lastName");
    let email = data.get("email");
    let password = data.get("password");

    // проверка на корректность введенных данных
    let regFirstName = regNames.test(firstName);
    let regLastName = regLast.test(lastName);
    let regPasswordCorrect = regPassword.test(password);
    let regEmailCorrect = regEmail.test(email);
    // если true, данные записываются в localstorage
    if (regFirstName && regLastName && regEmailCorrect && regPasswordCorrect) {
      const user = {
        firstName: data.get("firstName"),
        lastName: data.get("lastName"),
        email: data.get("email"),
        password: data.get("password"),
      };
      myStorage.setItem("user", JSON.stringify(user));

      navigate("/autorization");
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}></Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  error={name}
                  onChange={handleChangeFirstName}
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                  helperText="имя на латинице"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  error={lastName}
                  onChange={handleChangeLastName}
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="family-name"
                  helperText="фамилия на латинице"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  error={email}
                  onChange={handleChangeEmail}
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  helperText="емаил по форме test@mail.ru"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  error={password}
                  onChange={handleChangePassword}
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                  helperText="Пароль должен быть на латинице, содержать не менее 6 знаков, начинаться с заглавной буквы и содержать цифры"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Зарегистрироваться
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <NavLink
                  to="/autorization"
                  style={{ color: "blue" }}
                  variant="body2"
                >
                  Уже зарегистрированы? Войдите
                </NavLink>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>
  );
};

export default Registarion;
