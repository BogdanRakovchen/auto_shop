// -----------------react-------------------
import React, { useState } from "react";
// ---------------material ui ----------------
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import { TableHead } from "@mui/material";
// -------------------redux----------------------
import { stateAllCard, stateAllSum } from "../../redux/card/cardSlice";
import { useSelector } from "react-redux";

const CheckoutOrder = () => {
  const [showConfirm, setShowConfirm] = useState(false);

  const allCard = useSelector(stateAllCard);
  const allSum = useSelector(stateAllSum);

  const handleClickConfirm = () => {
    setShowConfirm(true);
  };

  if (!showConfirm) {
    return (
      <Box component="div">
        <Container>
          <Box component="h2">Ваш заказ на сумму: {allSum} рублей</Box>

          <Box component="div">
            <TableContainer component={Paper} sx={{ maxWidth: 600 }}>
              <Table
                sx={{ maxWidth: 600 }}
                aria-label="custom pagination table"
              >
                <TableHead>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Автомобиль
                    </TableCell>
                    <TableCell style={{ width: 160 }} align="center">
                      Цена
                    </TableCell>
                    <TableCell style={{ width: 160 }} align="right">
                      Количество
                    </TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {allCard.map((card) => (
                    <TableRow key={card.id}>
                      <TableCell component="th" scope="row" align="left">
                        {card.title}
                      </TableCell>
                      <TableCell component="th" scope="row" align="center">
                        {card.price}
                      </TableCell>
                      <TableCell align="right">{card.count}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
          <Box component="div" sx={{ marginTop: "50px" }}>
            <Button
              size="small"
              variant="contained"
              onClick={handleClickConfirm}
            >
              Подтвердить
            </Button>
          </Box>
        </Container>
      </Box>
    );
  } else {
    return (
      <Box component="div" sx={{ marginTop: "120px", width: "50%" }}>
        <Alert severity="success" width="50%">
          <AlertTitle>Успешно</AlertTitle>
          Спасибо! <strong>Ваш заказ принят в обработку</strong>
        </Alert>
      </Box>
    );
  }
};

export default CheckoutOrder;
