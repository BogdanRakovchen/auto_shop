// ---------react----------------
import React, { useState } from "react";
// --------material ui------------
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
// -----------redux-------------------
import { useDispatch } from "react-redux";
import { cardAdded } from "../../redux/card/cardSlice";
import { descriptionProduct } from "../../redux/product/productsSlice";
// -----------react-router-dom----------
import { Link } from "react-router-dom";

function Cards({ ...card }) {
  const { id, title, img, price, description } = card;

  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);

  const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });

  const addedCardItem = () => {
    dispatch(cardAdded(id, title, img, price));
    // при добавлении товара в корзину появляется уведомление
    setOpen(true);
  };

  // скрыть уведомление через 1500 ms
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const descrPorduct = () => {
    dispatch(descriptionProduct(card));
    let scrollElm = document.scrollingElement;
    scrollElm.scrollTop = 0;
  };

  return (
    <>
      <Snackbar open={open} autoHideDuration={1500} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success" sx={{ width: "100%" }}>
          Товар добавлен в корзину!
        </Alert>
      </Snackbar>

      <Card sx={{ maxWidth: 345, marginBottom: "20px" }}>
        <CardMedia component="img" height="250px" image={img} alt={title} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {description}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" onClick={addedCardItem}>
            Купить
          </Button>
          <Link to="/product" style={{ textDecoration: "none" }}>
            <Button onClick={descrPorduct} size="small">
              Подробнее
            </Button>
          </Link>
        </CardActions>
      </Card>
    </>
  );
}
export default Cards;
