// ------------react--------------
import React from "react";
// ------------material ui---------
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
// -----------redux-------------------
import { stateAllCard, stateAllSum } from "../../redux/card/cardSlice";
import { useSelector } from "react-redux";
// ----------Component---------------
import Order from "./Order";
// ----------react-router-dom------------
import { useNavigate } from "react-router-dom";

const BuyBasket = () => {
  const navigate = useNavigate();

  const allSum = useSelector(stateAllSum);

  const allCard = useSelector(stateAllCard);

  function checkNav() {
    navigate("/checkoutorder");
  }

  if (allCard.length !== 0) {
    return (
      <Box component="section" sx={{ width: "100%", marginBottom: "50px" }}>
        <Box component="h2">Корзина</Box>

        {/* второй блок справа */}

        <Box component="div" sx={{ width: "30%" }}>
          <Box
            component="div"
            sx={{
              display: "flex",
              flexDirection: "column",
              border: "1px solid",
              borderRadius: "7px",
              backgroundColor: "aliceblue",
              height: "150px",
            }}
          >
            <Box
              component="span"
              sx={{
                height: "50px",
                backgroundColor: "antiquewhite",
                textAlign: "center",
                fontSize: "1.5rem",
                fontWeight: "bold",
                paddingTop: "8px",
              }}
            >
              Итого: {allSum} рублей
            </Box>
            <Box
              component="div"
              sx={{
                display: "flex",
                justifyContent: "center",
                alignContent: "center",
                paddingTop: "26px",
              }}
            >
              <Button variant="contained" onClick={checkNav}>
                Оформить заказ
              </Button>
            </Box>
          </Box>
        </Box>

        {allCard.map((card) => {
          return <Order key={card.id} {...card} />;
        })}
      </Box>
    );
  } else {
    return (
      <Box
        component="div"
        sx={{
          width: "50%",
          height: "200px",
          margin: "0 auto",
          marginTop: "150px",
        }}
      >
        <Box
          component="h2"
          sx={{ textAlign: "center", fontSize: "50px", opacity: "0.2" }}
        >
          Корзина пуста
        </Box>
      </Box>
    );
  }
};

export default BuyBasket;
